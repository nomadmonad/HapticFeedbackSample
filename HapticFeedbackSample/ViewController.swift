import UIKit

class ViewController: UITableViewController {
    private enum Section {
        case impact
        case selection
        case notify
        case abuse

        init(_ index: Int) {
            switch index {
            case 0: self = .impact
            case 1: self = .selection
            case 2: self = .notify
            case 3: self = .abuse
            default: self = .impact
            }
        }
    }

    // https://developer.apple.com/design/human-interface-guidelines/ios/user-interaction/feedback/
    // https://developer.apple.com/documentation/uikit/animation_and_haptics
    private func impactfeedBackGenerator(_ style: UIImpactFeedbackGenerator.FeedbackStyle) -> UIImpactFeedbackGenerator {
        let generator = UIImpactFeedbackGenerator(style: style)
        // Preparing the generator can reduce latency when triggering feedback.
        // This is particularly important when trying to match feedback to sound or visual cues.
        generator.prepare()
        return generator
    }

    private func selectionfeedBackGenerator() -> UISelectionFeedbackGenerator {
        let generator = UISelectionFeedbackGenerator()
        generator.prepare()
        return generator
    }

    private func notificationFeedbackGenerator() -> UINotificationFeedbackGenerator {
        let generator = UINotificationFeedbackGenerator()
        generator.prepare()
        return generator
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch Section(indexPath.section) {
        case .impact:
            let style: UIImpactFeedbackGenerator.FeedbackStyle
            switch indexPath.row {
            case 0: style = .heavy
            case 1: style = .medium
            case 2: style = .light
            default: style = .light
            }

            let ifg = impactfeedBackGenerator(style)
            ifg.impactOccurred()
        case .selection:
            let sfg = selectionfeedBackGenerator()
            sfg.selectionChanged()
        case .notify:
            let style: UINotificationFeedbackGenerator.FeedbackType
            switch indexPath.row {
            case 0: style = .success
            case 1: style = .warning
            case 2: style = .error
            default: style = .error
            }

            let nfg = notificationFeedbackGenerator()
            nfg.notificationOccurred(style)
        case .abuse:
            DispatchQueue.global(qos: .background).async { [weak self] in
                let ifg = self?.impactfeedBackGenerator(.heavy)
                ifg?.impactOccurred()
            }
        }
    }
}

